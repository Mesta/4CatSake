# config valid only for current version of Capistrano
lock '3.5.0'

set :application, '4CatSake'
set :repo_url, 'git@gitlab.com:Mesta/4CatSake.git'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/mesta/www/4catsake'
set :linked_dirs, %w{node_modules}

# capistrano/laravel
set :laravel_set_linked_dirs, true
set :laravel_version, 5.2

# capistrano/file-permissions
# set :file_permissions_paths, ["storage"]
# set :file_permissions_groups, ["www-data"]
# set :file_permissions_chmod_mode, "0775"

# capistrano/npm
set :npm_flags, '--silent --no-progress'

namespace :deploy do
  before :reverted, 'npm:install'

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
