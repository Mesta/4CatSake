<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>4CatSake</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('css/business-frontpage.css') }}">

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- <a class="navbar-brand" href="#">4CatSake</a> -->
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <!-- <li>
                    <a href="#">Services</a>
                </li>
                <li>
                <a href="#">A propos</a>
            </li> -->
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</div>
<!-- /.container -->
</nav>

<!-- Image Background Page Header -->
<!-- Note: The background image is set within the business-casual.css file. -->
<header class="business-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="tagline">4CatSake</h1>
            </div>
        </div>
    </div>
</header>

<!-- Page Content -->
<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <h2>Notre ambition</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <p>
                Les organismes de protection des animaux font face à des difficultés
                dans leur organisation et leur communication interne et externe.
                Ce projet vise à mettre à disposition de ces organismes un outil moderne et performant
                afin qu'ils se concentrent plus aisément sur leur coeur de métier : la protection des animaux.
            </p>
            <p>
                Nous proposons d'accompagner les associations sur les sujets suivants :
                <ul>
                    <li>Suivi des entrées/sorties des animaux</li>
                    <li>Suivi et planification des soins d'animaux</li>
                    <li>Organisation des ommunication des évènements (exposition, portes ouvertes...)</li>
                    <li>Gestion des familles d'accueil</li>
                    <li>Gestion des ressources de l'association (membres, locaux, stocks...)</li>
                </ul>
            </p>
        </div>

        <div class="col-sm-4">
            <address>
                <strong>Vous souhaitez contribuer ?</strong>
                <br>Fork & PR : <a href="https://www.gitlab.com/Mesta/4CatSake">GitLab project</a>
                <br>Email : <a href="mailto:contact@mesta.fr">contact at mesta.fr</a>
            </address>
        </div>
    </div>
    <!-- /.row -->

    <hr>

    <div class="row">
        <div class="col-sm-12">
            <h1 class="text-center">Porteurs de projet</h1>
        </div>
    </div>

    <br/><br/>

    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="text-right">Jérémy Lardet</h3>
                    <p class="text-right">
                        Développeur web back-end chez <a href="https://www.gladys.com">Gladys</a>.<br/>
                        En savoir plus sur <a href="https://www.mesta.fr">mon site web perso</a>.
                    </p>
                </div>

                <div class="col-sm-4">
                    <img class="img-circle img-responsive img-center" src="images/jeremylardet.jpg" width="120px" alt="">
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-4">
                    <img class="img-circle img-responsive img-center" src="http://placehold.it/120x120" alt="">
                </div>

                <div class="col-sm-8">
                    <h3>Willy Poteloin</h3>

                    <p>
                        Développeur web back-end chez <a href="https://www.gladys.com">Gladys</a>.
                        Spécialiste de jQuery.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->

<!-- Footer -->
<footer>
    Copyright &copy; 4CatSake <?= date('Y') ?>
</footer>

<!-- jQuery -->
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

</body>

</html>
